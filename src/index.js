const express = require('express');
const path = require('path');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();

const {trucksRouter} = require('./controllers/trucksController');
const {loadsRouter} = require('./controllers/loadsController');
const {usersRouter} = require('./controllers/usersController'); 
const {authRouter} = require('./controllers/authController'); 
const {authMiddleware, driverMiddleware} = require('./middlewares/authMiddleware'); 
const {NodeCourseError} = require('./utils/errors'); 

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/trucks', [authMiddleware,driverMiddleware], trucksRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);
app.use('/api/users/me', [authMiddleware], usersRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'})
});

app.use((err, req, res, next) => {
    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://testuser:1111@cluster0.cxpqy.mongodb.net/test10?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();