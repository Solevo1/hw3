const { string } = require('joi');
const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assignedTo: {
        type: mongoose.Schema.Types.ObjectId,
        required: false
    },
    status: {
        type: String,
        required: true,
        default: 'NEW',
    },
    state: {
        type: String,
        required: true,
        default: 'En route to Pick Up',
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    pickup_address: {
        type: String,
        required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        type: Object,
        required: true,
    },
    logs: {
        type: Array,
        required: false,
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { Load };
