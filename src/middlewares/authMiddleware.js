const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');

const authMiddleware = (req, res, next) => {
    const {
        authorization
    } = req.headers;

    if (!authorization) {
        return res.status(401).json({message: 'Please, provide "authorization" header'});
    }

    const [, token] = authorization.split(' ');

    if (!token) {
        return res.status(401).json({message: 'Please, include token to request'});
    }

    try {
        const tokenPayload = jwt.verify(token, 'secret');
        req.user = {
            userId: tokenPayload._id,
            role: tokenPayload.role,
            email: tokenPayload.email,
            createdDate: tokenPayload.createdAt
        };
        next();
    } catch (err) {
        res.status(401).json({message: err.message});
    }
}

const driverMiddleware = async (req, res, next) => {
    const user = await User.findById(req.user.userId);
    if(user.role==="DRIVER") {
        next();
    } else {
        return res.status(400).json({message: 'You don`t have acess to this endpoint'});
    }
}

const shipperMiddleware = async (req, res, next) => {
    const user = await User.findById(req.user.userId);
    if(user.role==="SHIPPER") {
        next();
    } else {
        return res.status(400).json({message: 'You don`t have acess to this endpoint'});
    }
}

module.exports = {
    authMiddleware,
    driverMiddleware,
    shipperMiddleware
}