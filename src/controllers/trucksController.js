const express = require('express');
const { truckValidator } = require('../middlewares/validationMidlleware');
const router = express.Router();

const {
    addTruckToUser,
    getTrucksByUserId,
    assignTruckByIdForUser,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser
} = require('../services/trucksService');

const {
    asyncWrapper
} = require('../utils/apiUtils');


router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const trucks = await getTrucksByUserId(userId);
    res.json({trucks});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const truck = await getTruckByIdForUser(id, userId);
    res.json({truck});
}));

router.post('/', truckValidator, asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await addTruckToUser(userId, req.body);

    res.json({message: "Truck created successfully"});
}));

router.put('/:id', truckValidator, asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;
    await updateTruckByIdForUser(id, userId, data);
    res.json({message: "Truck updated successfully"});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    await assignTruckByIdForUser(id, userId);
    res.json({message: "Truck assigned successfully"});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    await deleteTruckByIdForUser(id, userId);
    res.json({message: "Truck deleted successfully"});
}));

module.exports = {
    trucksRouter: router
}