const express = require('express');
const router = express.Router();

const {
    deleteUserById,
    changePasswordById,
    getUserById
} = require('../services/usersService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const user = await getUserById(userId);
    res.json(user);
}));

router.delete('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await deleteUserById(userId);
    res.json({message: "User deleted successfully"});
}));

router.patch('/password', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const {oldPassword, newPassword} = req.body;
    await changePasswordById(userId,oldPassword,newPassword);
    res.json({message: "Password updated successfully"});
}));

module.exports = {
    usersRouter: router
}