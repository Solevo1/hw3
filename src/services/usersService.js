const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserById = async (userId) => {
    return await User.findById({ _id: userId }).select('-__v -password'); 
}

const deleteUserById = async (userId) => {
    await User.findByIdAndRemove(userId);
}

const changePasswordById = async (userId, oldPassword, newPassword) => {
    const user = await User.findById(userId);
    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new Error('Invalid password');
    }
    if(!newPassword) {
        throw new Error('Please enter new password');
    }
    await User.findByIdAndUpdate(userId,{ $set: {password:await bcrypt.hash(newPassword, 10)}});
}

module.exports = {
    deleteUserById,
    changePasswordById,
    getUserById
};