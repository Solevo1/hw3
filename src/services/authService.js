const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const nodemailer = require("nodemailer");
const randomstring = require("randomstring");

const {User} = require('../models/userModel');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'abobus.noreply@gmail.com',
        pass: 'roflanHlebalo'
    }
});

const registration = async ({email, password, role}) => {
    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role
    }); 
    await user.save();
}

const signIn = async ({email, password}) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new Error('Invalid email or password');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('Invalid email or password');
    }

    const token = jwt.sign({
        _id: user._id,
        email: user.email
    }, 'secret');
    return token;
}


const sendPasswordEmail = async (email) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new Error('Invalid email');
    }
    const temporaryPassword = randomstring.generate();

    await User.findByIdAndUpdate(user._id,{ $set: {password:await bcrypt.hash(temporaryPassword, 10)}});
    
    const mailOptions = {
        from: 'Delivery Service "ABOBUS"',
        to: email,
        subject: 'Password recovery',
        text: `HI! Seems like you forgot your password.
Here is your temporary password: ${temporaryPassword} .
Please change it in your account settings!`
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}


module.exports = {
    registration,
    signIn,
    sendPasswordEmail
};